import copy
import numpy as np

#https://en.wikipedia.org/wiki/Inverse_element#Matrices
def initalize_v(matrix):
    matrix_t = matrix.transpose()
    first_matrix_norm = np.linalg.norm(matrix, 1)
    inf_matrix_norm = np.linalg.norm(matrix, np.inf)
    to_return = matrix_t / (first_matrix_norm * inf_matrix_norm)
    return to_return

epsilon_precision = 10 ** (-6)

k_max = int(input("Insert K_max: "))
matrix_a = np.loadtxt("matrix_a")
choice = int(input("Insert choice:\n1 (Schultz)\n2 (Li si Li - 1)\n3 (Li si Li - 2)\n"))
n = len(matrix_a)
m = len(matrix_a[0])

transpose_matrix_a = 0
copy_matrix_a = copy.deepcopy(matrix_a)
if n != m:
    transpose_matrix_a = np.matrix.transpose(matrix_a)
    if n > m:
        matrix_a = np.dot(transpose_matrix_a, matrix_a)
    else:
        matrix_a = np.dot(matrix_a, transpose_matrix_a)

v_0 = initalize_v(matrix_a)
v_1 = initalize_v(matrix_a)
n = len(matrix_a)

identity_matrix = np.identity(n)
matrix_b = -matrix_a

pow_10 = 10 ** 10
k=0
delta_v = 0
while k <= k_max:
    v_0 = v_1
    if choice == 1:
        v_1 = np.dot(v_0,  np.add(np.dot(2, identity_matrix), np.dot(matrix_b, v_0)))
    elif choice == 2:
        v_1 = np.dot(
            v_0,
            np.add(
                np.dot(3, identity_matrix),
                np.dot(
                    matrix_b,
                    np.dot(
                        v_0,
                        np.add(
                            np.dot(3, identity_matrix),
                            np.dot(matrix_b, v_0)
                        )
                    )
                )
            )
        )
    elif choice == 3:
        v_1 = np.dot(
            np.add(identity_matrix,
                   np.dot(1.0/4,
                          np.dot(
                              np.subtract(identity_matrix, np.dot(v_0, matrix_a)),
                                np.dot(
                                    np.subtract(
                                        np.dot(3, identity_matrix),
                                        np.dot(v_0, matrix_a)
                                                ),
                                    np.subtract(
                                        np.dot(3, identity_matrix),
                                        np.dot(v_0, matrix_a)
                                                )
                                    )
                                )
                          )
                   ),
            v_0)
    else:
        raise NameError("Invalid Choice")

    delta_v = np.linalg.norm(np.subtract(v_1, v_0))
    k += 1
    if delta_v < epsilon_precision:
        break

    if delta_v > pow_10:
        break

print("Numarul iteratii: {}".format(k))
if delta_v < epsilon_precision:
    np.set_printoptions(suppress=True)
    n = len(copy_matrix_a)
    m = len(copy_matrix_a[0])
    if n == m:
        print("INV Matrix: \n{}".format(v_1))
        print("Norma: {}".format(np.linalg.norm(np.subtract(np.dot(matrix_a, v_1), identity_matrix), 1)))
    else:
        if n > m:
            inv_matrix = np.dot(v_1, transpose_matrix_a)
            print(np.dot(inv_matrix, copy_matrix_a))
            print("INV Matrix: \n{}".format(inv_matrix))
            print("Norma: {}".format(np.linalg.norm(np.subtract(np.dot(inv_matrix, copy_matrix_a), identity_matrix), 1)))
        else:
            inv_matrix = np.dot(transpose_matrix_a, v_1)
            print(np.dot(copy_matrix_a, inv_matrix))
            print("INV Matrix: \n{}".format(inv_matrix))
            print("Norma: {}".format(np.linalg.norm(np.subtract(np.dot(copy_matrix_a, inv_matrix), identity_matrix), 1)))
else:
    print("Divergenta")
