/*
	Made by: Irimia Cosmin & Bucevschi Alexandru [3A2]
*/
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toCollection;


public class Tema1 {

    public static Double c1, c2, c3, c4, c5, c6;
    public static Map<String, Integer> clasament;
    public static Map<Double, Set<String>> values;
    public static long timp1 = 0, timp2 = 0, timp3 = 0, timp4 = 0, timp5 = 0,  timp6 = 0, timp7 = 0;
    public static long startTime, endTime;

    static {
        c1 = 1.0 / (2.0*3.0);
        c2 = c1 * (1.0/(4.0*5.0));
        c3 = c2 * (1.0/(6.0*7.0));
        c4 = c3 * (1.0/(8.0*9.0));
        c5 = c4 * (1.0/(10.0*11.0));
        c6 = c5 * (1.0/(12.0*13.0));
        System.out.println("C1 = " + c1 + "\nC2 = " + c2 + "\nC3 = " + c3 + "\nC4 = " + c4 + "\nC5 = " + c5 + "\nC6 = " + c6 + "\n==============================\n");

        clasament = new HashMap<>();
        clasament.put("P1", 0);
        clasament.put("P2", 0);
        clasament.put("P3", 0);
        clasament.put("P4", 0);
        clasament.put("P5", 0);
        clasament.put("P6", 0);
        //clasament.put("Sin", 0);
        values = new HashMap<>();
    }

    public static Double P1(Double x){
        startTime = System.nanoTime();
        Double y = Math.pow(x, 2.0);
        Double descompunere = x * ( 1 - y * ( c1 - c2 * y) );
        //Double returnVal = x - c1 * Math.pow(x, 3.0) + c2 * Math.pow(x, 5.0);
        endTime = System.nanoTime();
        timp1 = timp1 + (endTime - startTime);
        return descompunere;
    }

    public static Double P2(Double x){
        startTime = System.nanoTime();
        Double y = Math.pow(x, 2.0);
        Double descompunere = x * ( 1 + y * ( -c1 + y * ( c2 - c3 * y) ) );
        //Double returnVal = x - c1 * Math.pow(x, 3.0) + c2 * Math.pow(x, 5.0) - c3 * Math.pow(x, 7.0);
        endTime = System.nanoTime();
        timp2 = timp2 + (endTime - startTime);
        return descompunere;
    }

    public static Double P3(Double x){
        startTime = System.nanoTime();
        Double y = Math.pow(x, 2.0);
        Double descompunere = x * ( 1 - y * ( c1 - y * ( c2 - y * ( c3 - c4 * y ) ) ) );
        //Double returnVal = x - 0.166 * Math.pow(x, 3.0) + 0.00833 * Math.pow(x, 5.0) - c3 * Math.pow(x, 7.0) + c4 * Math.pow(x, 9.0);
        endTime = System.nanoTime();
        timp3 = timp3 + (endTime - startTime);
        return descompunere;
    }

    public static Double P4(Double x){
        startTime = System.nanoTime();
        Double y = Math.pow(x, 2.0);
        Double descompunere = x * ( 1 - y * ( 0.166 - y * ( 0.00833 - y * ( c3 - c4 * y) ) ) );
        //Double returnVal = x - c1 * Math.pow(x, 3.0) + c2 * Math.pow(x, 5.0) - c3 * Math.pow(x, 7.0) + c4 * Math.pow(x, 9.0);
        endTime = System.nanoTime();
        timp4 = timp4 + (endTime - startTime);
        return descompunere;
    }

    public static Double P5(Double x){
        startTime = System.nanoTime();
        Double y = Math.pow(x, 2.0);
        Double descompunere = x * ( 1 - y * ( c1 - y * ( c2 - y * ( c3 - y * ( c4  - c5 * y )) ) ) );
        //Double returnVal = x - c1 * Math.pow(x, 3.0) + c2 * Math.pow(x, 5.0) - c3 * Math.pow(x, 7.0) + c4 * Math.pow(x, 9.0) - c5 * Math.pow(x, 11.0);
        endTime = System.nanoTime();
        timp5 = timp5 + (endTime - startTime);
        return descompunere;
    }

    public static Double P6(Double x){
        startTime = System.nanoTime();
        Double y = Math.pow(x, 2.0);
        Double descompunere = x * ( 1 - y * ( c1 - y * ( c2 - y * ( c3 - y * ( c4  - y * ( c5 - c6 * y ) )) ) ) );
        //Double returnVal = x - c1 * Math.pow(x, 3.0) + c2 * Math.pow(x, 5.0) - c3 * Math.pow(x, 7.0) + c4 * Math.pow(x, 9.0) - c5 * Math.pow(x, 11.0) + c6 * Math.pow(x, 13.0);
        endTime = System.nanoTime();
        timp6 = timp6 + (endTime - startTime);
        return descompunere;
    }

    public static Double P7(Double x){
        startTime = System.nanoTime();
        Double descompunere = Math.sin(x);
        endTime = System.nanoTime();
        timp7 = timp7 + (endTime - startTime);
        return descompunere;
    }

    public static Double getU(){
        Double m = 1.0;
        Double u = Math.pow(10,-m);
        while((1 + u) != 1){
            u = Math.pow(10,-m);
            m++;
        }
        return Math.pow(10,-(m-2));
    }

    public static boolean asociativitateAdunare(Double x, Double y, Double z){
        return (x+y) + z == x + (y+z);
    }

    public static boolean asociativitateInmultire(Double x, Double y, Double z){
        return (x*y) * z == x * (y*z);
    }

    public static Double eroare(Double pX, Double x){
        return Math.abs(pX - Math.sin(x));
    }

    public static Set<String> best3Polys(Map<String, Double> erori){
        Map<String, Double> sortedMap = erori.entrySet().stream()
                        .sorted(Map.Entry.comparingByValue())
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                                (e1, e2) -> e2-e1, LinkedHashMap::new));
        return sortedMap.keySet().stream().limit(3).collect(toCollection(LinkedHashSet::new));
    }

    private static void makeClasament(){
        for(Double key: values.keySet()){
            List<String> list = new ArrayList<String>(values.get(key));
            for(int i = 0; i< list.size(); i++){
                clasament.put(list.get(i), clasament.get(list.get(i)) + (3-i));
                //System.out.println(list.get(i) + " " + clasament.get(list.get(i)) + " " + i);
            }
        }

        Map<String, Integer> sortedMap = clasament.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (e1, e2) -> e1-e2, LinkedHashMap::new));
        System.out.println("\nClasament Polinoame\n==============================\n");
        List<String> outList = new ArrayList<>();
        sortedMap.forEach((key, value) -> {
            outList.add("Polinomul " + key + " are un punctaj de " + value);
        });
        Collections.reverse(outList);
        outList.forEach((s) -> System.out.println(s));
    }

    private static void addValuesForX(Double x){
        Map<String, Double> erori = new HashMap<>();
        Double p1 = P1(x), p2 = P2(x), p3 = P3(x), p4 = P4(x), p5 = P5(x), p6 = P6(x), p7 = P7(x);
        System.out.println("Valori ("+x+") : " + p1 + " " + p2 + " " + p3 + " " + p4 + " " + p5 + " " + p6 );

        erori.put("P1", eroare(p1,x));
        erori.put("P2", eroare(p2,x));
        erori.put("P3", eroare(p3,x));
        erori.put("P4", eroare(p4,x));
        erori.put("P5", eroare(p5,x));
        erori.put("P6", eroare(p6,x));
       // erori.put("Sin", eroare(p7, x));
        System.out.println("Erori (" + x + ") = " + erori + "\n");
        values.put(x, best3Polys(erori));
    }

    public static void printTimes(){
        List<Long> times = new ArrayList<>();
        times.add(timp1);
        times.add(timp2);
        times.add(timp3);
        times.add(timp4);
        times.add(timp5);
        times.add(timp6);
        //times.add(timp7);
        System.out.println("\n ========== TIMPI ==========\n");
        Collections.sort(times);
        for(Long time: times)
        {
            if(time == timp1){
                System.out.println("Polinomul P1 s-a calculat timp de " + time + " nanosecunde;");
            }else if(time == timp2){
                System.out.println("Polinomul P2 s-a calculat timp de " + time + " nanosecunde;");
            }else if(time == timp3){
                System.out.println("Polinomul P3 s-a calculat timp de " + time + " nanosecunde;");
            }else if(time == timp4){
                System.out.println("Polinomul P4 s-a calculat timp de " + time + " nanosecunde;");
            }else if(time == timp5){
                System.out.println("Polinomul P5 s-a calculat timp de " + time + " nanosecunde;");
            }else if(time == timp6){
                System.out.println("Polinomul P6 s-a calculat timp de " + time + " nanosecunde;");
            }else if(time == timp7){
                System.out.println("Polinomul SIN s-a calculat timp de " + time + " nanosecunde;");
            }
        }
    }

    public static void ex3(){
        Random rand = new Random();
        Double MIN = - ( Math.PI / 2);
        Double MAX = Math.PI / 2;
        for(int i=1; i<11; i++){
            Double randValue = MIN + (MAX - MIN) * rand.nextDouble();
            addValuesForX(Double.parseDouble(String.valueOf(i)));
        }
        makeClasament();
    }

    public static void main(String[] argv){
        Double u = getU();
        System.out.println("Ex1: U este " + u + "\n");
        System.out.println("Ex2: Este asociativa la adunare? : " + asociativitateAdunare(1.0, u, u) + "\n");
        System.out.println("Ex2: Este asociativa la inmultire? : " + asociativitateInmultire(Math.pow(u, 3), Math.pow(u, 6), Math.pow(u, 7)) + "\n");
        ex3();
        printTimes();

    }
}
