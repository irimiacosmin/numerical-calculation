/*
	Made by: Irimia Cosmin & Bucevschi Alexandru [3A2]
*/
import Jama.Matrix;

import java.io.File;
import java.util.Scanner;

public class Tema5{

    public static final String prefix = "src/main/java/";
    public static Double epsilon = Math.pow(10, -6);
    public static Double pow10 = Math.pow(10, 10);

    public static Matrix matrix_A;
    public static Matrix matrix_B;
    public static Matrix matrix_A_copy;
    public static Matrix matrix_A_transpose;

    public static Matrix matrix_A_v1;
    public static Matrix matrix_A_v0;
    public static Matrix matrix_A_identity;
    public static Matrix inv_matrix;

    public static Integer K_Max;
    public static Integer choice;
    public static Integer N;
    public static Integer M;
    public static Integer K = 0;
    public static Double delta_V;


    public static Matrix initialize(Matrix matrix){
        Matrix auxMatrix = matrix.transpose();
        Double firstMatrixNorm = matrix.norm1();
        Double infMatrixNorm = matrix.normInf();
        return auxMatrix.arrayLeftDivide(new Matrix(1,1,firstMatrixNorm * infMatrixNorm));
    }

    public static double[][] readMatrixFromFile(String filename, int rows, int columns){
        double[][] a = new double[rows][columns];
        try{
            Scanner input = new Scanner(new File(prefix + filename));
            for(int i = 0; i < rows; ++i)
            {
                for(int j = 0; j < columns; ++j)
                {
                    if(input.hasNextInt())
                    {
                        a[i][j] = input.nextDouble();
                    }
                }
            }
        }catch (Exception e){
            e.fillInStackTrace();
        }
        return a;
    }


    public static void case_One() {
        matrix_A_v1 = matrix_A_v0.times(
                matrix_A_identity.times(2).plus(
                        matrix_B.times(matrix_A_v0)
                )
        );
    }

    public static void case_Two() {
        matrix_A_v1 = matrix_A_v0.plus(
                matrix_A_identity.times(3).plus(
                        matrix_B.times(
                                matrix_A_v0.plus(
                                        matrix_A_identity.times(3).plus(
                                                matrix_B.times(matrix_A_v0)
                                        )
                                )
                        )
                )
        );
    }

    public static void case_Three() {
        matrix_A_v1 = matrix_A_identity.plus(
                matrix_A_identity.minus(matrix_A_v0).times(
                        matrix_A_identity.times(3).minus(
                                matrix_A_v0.minus(matrix_A).times(
                                        matrix_A_identity.times(3).minus(
                                                matrix_A_v0.times(matrix_A)
                                        )
                                )
                        )
                ).times(1.0/4)
        ).times(matrix_A_v0);
    }

    public static void main(String[] args) {
        matrix_A = new Matrix(readMatrixFromFile("matrix_a",6,5));
        N = matrix_A.getRowDimension();
        M = matrix_A.getColumnDimension();
        Scanner in = new Scanner(System.in);
        System.out.println("Insert K_max: ");
        K_Max = in.nextInt();
        System.out.println("Insert choice:\n1 (Schultz)\n2 (Li si Li - 1)\n3 (Li si Li - 2)\n");
        choice = in.nextInt();

        matrix_A_copy = matrix_A.copy();
        matrix_A_transpose = new Matrix(0,0);
        if (N != M) {
            matrix_A_transpose = matrix_A.transpose();
            if (N>M){
                matrix_A = matrix_A.times(matrix_A_transpose);
            }else {
                matrix_A = matrix_A_transpose.times(matrix_A);
            }
        }

        matrix_A_v0 = initialize(matrix_A);
        matrix_A_v1 = initialize(matrix_A);
        matrix_A_identity = new Matrix(N,N,0);
        matrix_B = matrix_A.times(-1);


        while (K < K_Max){
            matrix_A_v0 = matrix_A_v1;
            switch (choice){
                case 1:{
                    case_One();
                    break;
                }
                case 2:{
                    case_Two();
                    break;
                }
                case 3:{
                    case_Three();
                    break;
                }
                default:{
                    break;
                }
            }
        }

        delta_V = matrix_A_v1.minus(matrix_A_v0).norm1();
        K+=1;
        if(delta_V < epsilon || delta_V > pow10){
            return;
        }

        System.out.println("Numarul de iteratii: ");

        if(delta_V < epsilon){

            N = matrix_A_copy.getRowDimension();
            M = matrix_A_copy.getColumnDimension();

            if(N.equals(M)){

                System.out.println("INV Matrix: " + matrix_A_v1);

            }else{

                if(N>M){

                    inv_matrix = matrix_A_v1.times(matrix_A_transpose);
                    System.out.println(inv_matrix.times(matrix_A_copy));
                    System.out.println("INV Matrix: " + inv_matrix);
                    System.out.println("Norma: " + inv_matrix.times(matrix_A_copy.minus(matrix_A_identity).norm1()));


                }else{

                    inv_matrix = matrix_A_transpose.times(matrix_A_v1);
                    System.out.println(matrix_A_copy.times(inv_matrix));
                    System.out.println("INV Matrix: " + inv_matrix);
                    System.out.println("Norma: " + matrix_A_copy.times(inv_matrix.minus(matrix_A_identity).norm1()));

                }

            }

        }else{
            System.out.println("Divergenta");
        }
    }


}
