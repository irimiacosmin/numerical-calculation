import math
import random

near_interval = 0.2
epsilon = 10 ** (-8)
kmax = 100000
global_h = 10 ** -5


def check_different_values(value1, value2):
    if abs(value1 - value2) > epsilon:
        return True
    return False


def read_coef(input_file):
    handler = open(input_file, "r")
    content = handler.read()
    handler.close()

    items = content.split(" ")
    items = [int(x.strip()) for x in items if x.strip() != '']

    return items


def compute_interval(items):
    a = abs(max(items, key=abs))
    r = (abs(items[0]) + a) / items[0]
    return [-r, r]


def compute_poly(coef, x):
    s = 0
    coef.reverse()
    for i in range(0, len(coef)):
        s += x ** i * coef[i]
    return s


def muller(desired_value, interval, coef):
    k = 3
    left_value = max(interval[0], desired_value - near_interval)
    right_value = min(interval[1], desired_value + near_interval)
    x0 = random.uniform(left_value, right_value)
    x1 = random.uniform(left_value, right_value)
    x2 = random.uniform(left_value, right_value)
    #print("Am ales initial x0={}, x1={}, x2={}".format(x0, x1, x2))
    deltax = 0
    while True:
        h0 = x1 - x0
        h1 = x2 - x1

        delta0 = (compute_poly(coef, x1) - compute_poly(coef, x0)) / h0
        delta1 = (compute_poly(coef, x2) - compute_poly(coef, x1)) / h1

        a = (delta1 - delta0) / (h0 + h1)
        b = a * h1 + delta1
        c = compute_poly(coef, x2)

        if b ** 2 - 4 * a * c < 0:
            break

        rad1 = b + math.sqrt(b ** 2 - 4 * a * c)
        rad2 = b - math.sqrt(b ** 2 - 4 * a * c)

        if abs(max(rad1, rad2)) < epsilon:
            break

        deltax = 2 * c / max(rad1, rad2)

        x3 = x2 - deltax
        k += 1
        x0 = x1
        x1 = x2
        x2 = x3

        if abs(deltax) < epsilon or k > kmax or abs(deltax) > 10 ** 8:
            break

    if abs(deltax) < epsilon:
        #print("Solution: {}".format(x2))
        return x2
    else:
        print("Divergenta...")
        return None


def compute_f(x):
    return x ** 2 - 4 * x + 3
    # return x * 2 + math.exp(x)
    # return x ** 4 - 6 * x ** 3 + 13 * x ** 2 - 12 * x + 4


def compute_first_derivate_1(x):
    return (3 * compute_f(x) - 4 * compute_f(x - global_h) + compute_f(x - 2 * global_h)) / (2 * global_h)
    # return (3 * compute_f(x) - 4 * compute_f(x - global_h) + compute_f(x - 2 * global_h)) / 2 * global_h


def compute_first_derivate_2(x):
    return (- compute_f(x + 2 * global_h) + 8 * compute_f(x + global_h) - 8 * compute_f(x - global_h) + compute_f(
        x - 2 * global_h)) / (12 * global_h)


def compute_second_derivate(x):
    return (-compute_f(x + 2 * global_h) + 16 * compute_f(x + global_h) - 30 * compute_f(x) + 16 * compute_f(
        x - global_h) - compute_f(x - 2 * global_h)) / (12 * global_h ** 2)


def secanta(desired_value, derivate):
    left_value = desired_value - near_interval
    right_value = desired_value + near_interval
    x0 = random.uniform(left_value, right_value)
    x1 = random.uniform(left_value, right_value)
    k = 0

    deriv_list = [compute_first_derivate_1, compute_first_derivate_2]
    deriv = deriv_list[derivate]

    while True:

        if deriv(x1) - deriv(x0) >= -epsilon and deriv(x1) - deriv(x0) <= epsilon:
            deltax = 10 ** (-5)
        else:
            deltax = (x1 - x0) * deriv(x1) / (deriv(x1) - deriv(x0))

        x0 = x1
        x1 = x1 - deltax
        k += 1

        if abs(deltax) < epsilon or k > kmax or abs(deltax) > 10 ** 8:
            break


    if abs(deltax) < epsilon:
        #print("Solution: {}".format(x1))
        return x1
    else:
        print("Divergenta...")
        return None


def hor_(coef, x):
    b = coef[0]
    for i in range(1, len(coef)):
        b = coef[i] + b * x
    return b


def main():
    coef = read_coef("input")
    interval = compute_interval(coef)
    print("Interval: [{}, {}]".format(interval[0], interval[1]))
    results = set()
    for i in range(0, 2):
        result = None
        value = muller(2, interval, coef)
        ok = True
        for i in results:
            if check_different_values(i, value) == False:
                ok = False
                break

        if ok == True:
            results.add(value)

        if value != None:
            result = hor_(coef, value)
        print("Value: {} | Function value: {}".format(value, result))

    handler = open("output", "w")
    for item in results:
        handler.write("{}\n".format(item))
    handler.close()

    x1 = secanta(2, 0)
    x2 = secanta(2, 1)

    print("Secanta using first derivate - method 1: {}".format(x1))
    print("check is min with second derivate: {}".format(compute_second_derivate(x1) > 0))
    print("Secanta using first derivate - method 2: {}".format(x2))
    print("check is min with second derivate: {}".format(compute_second_derivate(x2) > 0))


if __name__ == '__main__':
    main()
