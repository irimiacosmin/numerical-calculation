import random
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

recursive_list = []


def compute_function(x):
    return x ** 4 - 12 * x ** 3 + 30 * x ** 2 + 12


def read_data_from_file(filename):
    handler = open(filename, 'r')
    content = handler.readlines()
    handler.close()
    n = int(content[0].strip())
    x0 = int(content[1].strip())
    xn = int(content[2].strip())

    if x0 >= xn:
        raise NameError("Invalid x0, xn")

    x_values = list()
    y_values = list()

    h = (xn-x0 + 1) / n

    x_values.append(x0)
    y_values.append(compute_function(x0))

    for counter in range(1, n):
        x = x0 + counter * h
        x_values.append(x)
        y_values.append(compute_function(x))

    return x_values, y_values, h


def aitken(y_values):

    final_list_values = list()
    final_list_values.append(y_values[0])

    delta_f = list()
    for i in range(1, len(y_values)):
        delta_f.append(y_values[i] - y_values[i-1])
    final_list_values.append(delta_f[0])

    """Compute k"""
    for k in range(1, len(y_values)):
        new_delta_f = list()
        for j in range(1, len(y_values) - k):
            new_delta_f.append(delta_f[j] - delta_f[j-1])

        if len(new_delta_f):
            final_list_values.append(new_delta_f[0])

        delta_f = new_delta_f

    return final_list_values


def recursive_s(n, t):
    if n <= 1:
        recursive_list.append(float(t))
        return t * 1.0

    s = float((t - n + 1.0) / n / 1.0 * recursive_s(n-1, t))
    recursive_list.append(s)
    return s


def aprox_x(x, x_values, h):
    t = (x - x_values[0]) / h
    global recursive_list
    recursive_list = []
    recursive_s(len(x_values), t)
    aitken_values = aitken(y_values)

    s = 0

    for i in range(0, len(y_values)):
        if i == 0:
            s += aitken_values[0]
        else:
            s += aitken_values[i] * recursive_list[i-1]

    return s


def interpolation_squares(x0, x_values, y_values):
    size = len(y_values)
    size = int(size / 2)
    B = []
    Rez = []
    for i in range(0, size):
        temp = list()
        for j in range(0, size):
            temp.append(0)
        B.append(temp)
        Rez.append(0)

#    facem un polinom s(x) = x^n + x^n-1 ... + x^1 + c
    for i in range(0, size):
        for j in range(0, size):
            B[i][j] = x_values[i] ** (size - j - 1)
        Rez[i] = y_values[i]

    a = np.linalg.solve(B, Rez)

    d = a[0]
    for i in range(1, len(a)):
        d = a[i] + d * x0

    return d


def plot_function(x_values, y_values, h):
    min_v = min(x_values)
    max_v = max(x_values)
    xt = set()
    f_function = []
    n_function = []
    sm_function = []

    for i in range(0, 100):
        xt.add(random.uniform(min_v, max_v))

    xt = sorted(list(xt))

    for x in xt:
        f_function.append(compute_function(x))
        n_function.append(aprox_x(x, x_values, h))
        sm_function.append(interpolation_squares(x, x_values, y_values))

    print(xt)
    print(f_function)
    print(n_function)
    print(sm_function)


    plt.cla()
    flg = plt.figure()
    #flg.set_size_inches(18.5, 10.5, forward=True)
    plt.plot(xt, f_function, color='b', marker='.')
    plt.plot(xt, n_function, color='r', marker='.')
    plt.plot(xt, sm_function, color='g', marker='.')
    flg.savefig("representation")



if __name__ == '__main__':
    x_values, y_values, h = read_data_from_file("input")
    print(x_values, y_values)
    aprox_value = aprox_x(50, x_values, h)
    real_value = compute_function(50)
    print("Ln(x): {} | Norm: |Ln(x) - f(x)|: {}".format(aprox_value,
                                                        np.linalg.norm(aprox_value-real_value)))
    aprox_sm_value = interpolation_squares(50, x_values, y_values)
    print("Sm(x): {} | Norm: |Sm(x) - f(x)|: {}".format(aprox_sm_value, np.linalg.norm(aprox_sm_value - real_value)))
    plot_function(x_values, y_values, h)
