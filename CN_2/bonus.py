import re

epsilon = 10 ** (-8)
above_diag, diag, under_diag, results = [[float(m) for m in re.findall("([\+\-]?[\d\.]+)", l)] for l in
                                         open("diagonals.txt").read().split("\n") if len(l) > 0]
# print(above_diag)
under_diag = [0] + under_diag
above_diag = above_diag + [0]
results += [0]

dim = len(diag)

for i in range(dim - 1):
    if abs(under_diag[i + 1]) < epsilon: continue
    multipl = diag[i] / under_diag[i + 1]
    under_diag[i + 1] = 0
    diag[i + 1] = multipl * diag[i + 1] - above_diag[i]
    above_diag[i + 1] *= multipl
    results[i + 1] = multipl * results[i + 1] - results[i]

for i in range(dim - 1, -1, -1):
    if abs(diag[i]) < epsilon:
        print("matrice singulara")
        exit()

    results[i] = (results[i] - above_diag[i] * results[i + 1]) / diag[i]
    diag[i] = 1
    above_diag[i] = 0

results = results[:-1]

print(results)


# 3.0 3 3.0 3
# 2 2 2 2 2
# 4 4 4 4

# 5 9 9 9 6
