import sys

import numpy as np
n = 3 #int(input("Insert the n dimension: "))
e = 3 #int(input("Insert e, where e is 10**(-e): "))

matrix_a = np.loadtxt("matrix_a")
matrix_b = np.loadtxt("matrix_b")

matrix_b = np.reshape(matrix_b, (len(matrix_b), 1))
epsilon = 10 ** (-e)
init_matrix_a = matrix_a.copy()
init_matrix_b = matrix_b.copy()


def diagonal_matrix(dim):
    i = np.zeros((dim, dim))
    np.fill_diagonal(i, 1)
    return i


def multiply_line(matrix, line, coef):
    i = diagonal_matrix(len(matrix))
    i[line][line] = coef
    return np.dot(i, matrix)


def switch_lines(matrix, l1, l2):
    i = diagonal_matrix(len(matrix))
    i[l1][l1] = i[l2][l2] = 0
    i[l1][l2] = i[l2][l1] = 1
    return np.dot(i, matrix)


def diff_lines(matrix, l1, l2):
    matrix[l2] -= matrix[l1]
    return matrix


def choose_pivot(matrix, e):
    dim = len(matrix)
    lista_pivoti = [abs(matrix[l][e]) for l in range(e, dim)]
    return e + np.unravel_index(np.argmax(lista_pivoti), np.shape(lista_pivoti))[0]

def algoritm():
    global matrix_a, matrix_b
    dim = len(matrix_a)
    for line in range(dim):
        pivot = choose_pivot(matrix_a, line)
        print(pivot)
        if abs(matrix_a[pivot][line]) <= epsilon:
            print("Matrice singulara!!")
            exit()
        matrix_a = switch_lines(matrix_a, line, pivot)
        matrix_b = switch_lines(matrix_b, line, pivot)
        sys.stdout.flush()
        for sline in range(line + 1, dim):
            m = matrix_a[line][line] / matrix_a[sline][line]
            matrix_a = multiply_line(matrix_a, sline, m)
            matrix_b = multiply_line(matrix_b, sline, m)
            matrix_a = diff_lines(matrix_a, line, sline)
            matrix_b = diff_lines(matrix_b, line, sline)
    x = [0 for _ in range(dim)]
    for i in range(dim - 1, -1, -1):
        x[i] = (matrix_b[i] - sum([matrix_a[i][j] * x[j] for j in range(i + 1, dim)])) / matrix_a[i][i]
    return x


def validate(x, matrix_a, matrix_b):
    result = np.dot(matrix_a, x) - matrix_b
    return np.linalg.norm(result, 2)


result = algoritm()
print("Solution: {}".format(result))
norm_result = validate(result, init_matrix_a, init_matrix_b)
print("Validation: {}".format(norm_result))
xlib = np.linalg.solve(init_matrix_a, init_matrix_b)
print("NP Result: {}".format(xlib))
custom_diff = np.array(result)-xlib
print("First result: {}".format(np.linalg.norm(custom_diff, 2)))
inv = np.linalg.inv(init_matrix_a)
print("NO Inv: {}".format(inv))
custom_diff = np.array(result) - np.dot(inv, init_matrix_b)
print("Second result: {}".format(np.linalg.norm(custom_diff, 2)))



