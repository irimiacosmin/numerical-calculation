/*
	Made by: Irimia Cosmin & Bucevschi Alexandru [3A2]
*/

import Jama.Matrix;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Tema7 {

    public static final String prefix = "src/main/java/";

    public static List<Float> recursiveList;
    public static List<Integer> xValues;
    public static List<Integer> yValues;

    public static Integer n;
    public static Integer x0;
    public static Integer xn;
    public static Integer h;


    public static Integer compute_function(Integer x){
        return Integer.parseInt(String.valueOf(Math.pow(x.doubleValue(),4) - 12 *
                Math.pow(x.doubleValue(),3) + + 30 *
                Math.pow(x.doubleValue(),2) + 12));
    }

    public static void readMatrixFromFile(String filename){
        List<String> lines = new ArrayList<String>();
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(prefix + filename));
            String line = reader.readLine();
            while (line != null) {
                lines.add(line);
                line = reader.readLine();
            }
            reader.close();

            n = Integer.parseInt(lines.get(0));
            x0 = Integer.parseInt(lines.get(1));
            xn = Integer.parseInt(lines.get(2));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<Integer> aitken(List<Integer> yValues){
        List<Integer> finalListValues = new ArrayList<Integer>();
        finalListValues.add(yValues.get(0));

        List<Integer> deltaF = new ArrayList<Integer>();

        for (int i = 1; i < yValues.size(); i++){
            deltaF.add(yValues.get(i) - yValues.get(i-1));
        }

        finalListValues.add(deltaF.get(0));

        for (int k = 1; k < yValues.size(); k++){
            List<Integer> newDeltaF = new ArrayList<Integer>();
            for (int j = 1; j < yValues.size() - k; j++){
                newDeltaF.add(deltaF.get(j) - deltaF.get(j-1));
            }
            if (newDeltaF.size() > 0){
                finalListValues.add(newDeltaF.get(0));
            }
            deltaF = newDeltaF;
        }
        return finalListValues;
    }

    public static Double recursiveS(Integer n, Integer t){
        if (n<=1){
            recursiveList.add(t.floatValue());
            return t * 1.0;
        }

        s = (float)((t - n + 1.0) / n / 1.0 * recursiveS(n-1, t));
        recursiveList.add(s);
        return s;
    }

    public static Integer aproximationX(Integer x, List<Integer> xValues, Integer h){
        Integer t = (x - xValues.get(0)) / h;
        recursiveList = new ArrayList<Float>();
        recursiveS(xValues.size(), t);
        List<Integer> aitkenValues = aitken(yValues);

        Integer s = 0;

        for (int i = 0; i < yValues.size(); i++){
            if (i == 0){
                s+= aitkenValues.get(0);
            }else{
                s+= aitkenValues.get(i) * Integer.parseInt(recursiveList.get(i-1).toString());
            }
        }
        return s;
    }

    public static Integer interpolationSquares(Integer x, List<Integer> xValues, List<Integer> yValues) {

        Integer size = yValues.size() / 2;

        List<List<Integer>> bAux = new ArrayList<List<Integer>>();
        List<Integer> rezAux = new ArrayList<Integer>();

        for (int i = 0; i < size; i++) {
            List<Integer> temp = new ArrayList<Integer>();
            for (int j = 0; j < size; j++) {
                temp.add(0);
            }
            bAux.add(temp);
            rezAux.add(0);
        }

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                List<Integer> temp = bAux.get(i);
                Integer auxSet = size - j - 1;

                temp.set(j, Integer.valueOf(
                        String.valueOf(Math.pow(
                                Double.valueOf(xValues.get(i).toString()),
                                Double.valueOf(auxSet.toString())
                                )
                        )
                        )
                );
            }
            rezAux.set(i, yValues.get(i));
        }
        List<Integer> d = bAux.get(0);
        Integer auxReturn = d.get(0);
        for (int i = 1; i < d.size(); i++){
            auxReturn = d.get(i) + auxReturn * x0;
        }
        return auxReturn;
    }

    public static void plotFunction(List<Integer> xValues, List<Integer> yValues){
        Integer min = Collections.min(xValues);
        Integer max = Collections.max(xValues);

        Set<Integer> xt = new TreeSet<Integer>();
        List<Integer> fFunction = new ArrayList<Integer>();
        List<Integer> nFunction = new ArrayList<Integer>();
        List<Integer> smFunction = new ArrayList<Integer>();

        Random random = new Random();

        for (int i=0; i<100; i++){
            xt.add(random.nextInt(max - min) - min);
        }

        for(Integer x : xt){
            fFunction.add(compute_function(x));
            nFunction.add(aproximationX(x, xValues, h));
            smFunction.add(interpolationSquares(x, xValues, yValues));
        }

        System.out.println(xt);
        System.out.println(fFunction);
        System.out.println(nFunction);
        System.out.println(smFunction);
    }

    public static void main(String[] args) {

        recursiveList = new ArrayList<Float>();
        xValues = new ArrayList<Integer>();
        yValues = new ArrayList<Integer>();

        readMatrixFromFile("input");
        if(x0 >= xn){
            System.out.println("Invalid x0, xn");
            return;
        }

        h = (xn-x0 + 1) / n;
        xValues.add(x0);
        yValues.add(compute_function(x0));

        for(int counter = 1; counter<n; counter++){
            x = x0 + counter * h;
            xValues.add(x);
            yValues.add(compute_function(x));
        }

        System.out.println(xValues);
        System.out.println(yValues);

        Integer aproxValue = aproximationX(50, xValues, h);
        Integer realValue = compute_function(50);

        System.out.println("Ln(x): " + aproxValue);
        System.out.println("Norm |Ln(x) - f(x)|: " + new Matrix(1,1, aproxValue - realValue).norm1());

        Integer aproxSmValue = interpolationSquares(50, xValues, yValues);

        System.out.println("Sm(x): " + aproxSmValue);
        System.out.println("Norm |Sm(x) - f(x)|: " + new Matrix(1,1, aproxSmValue - realValue).norm1());

        plotFunction(xValues, yValues);
    }
}
