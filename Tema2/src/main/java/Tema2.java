/*
	Made by: Irimia Cosmin & Bucevschi Alexandru [3A2]
*/
import Jama.Matrix;

import java.io.File;
import java.util.*;

public class Tema2 {

    public static Integer N;
    public static Integer E;
    public static Matrix matrix_A;
    public static Matrix matrix_B;
    public static Matrix init_matrix_A;
    public static Matrix init_matrix_B;
    public static String prefix = "src/main/java/";
    public static Double epsilon;

    public static double[][] readMatrixFromFile(String filename, int rows, int columns){
        double[][] a = new double[rows][columns];
        try{
            Scanner input = new Scanner(new File(prefix + filename));
            for(int i = 0; i < rows; ++i)
            {
                for(int j = 0; j < columns; ++j)
                {
                    if(input.hasNextInt())
                    {
                        a[i][j] = input.nextDouble();
                    }
                }
            }
        }catch (Exception e){
            e.fillInStackTrace();
        }
        return a;
    }

    public static Matrix makeDiagonalMatrix(int dimension){
        double[][] a = new double[dimension][dimension];
        for(int i = 0; i < dimension; ++i)
        {
            for(int j = 0; j < columns; ++j)
            {
                if(i==j){
                    a[i][j] = 1;
                }else{
                    a[i][j] = 0;
                }
            }
        }
        return new Matrix(a);
    }

    public static Matrix multiplyLine(Matrix matrix, Integer line, Integer coef){
        Matrix matrixAux = makeDiagonalMatrix(Math.min(matrix.getRowDimension(), matrix.getColumnDimension()));
        matrixAux.set(line, line, coef);
        return matrixAux.times(matrix);
    }

    public static Matrix switchLines(Matrix matrix, Integer lineOne, Integer lineTwo){
        Matrix matrixAux = makeDiagonalMatrix(Math.min(matrix.getRowDimension(), matrix.getColumnDimension()));
        matrixAux.set(lineOne, lineOne, 0);
        matrixAux.set(lineTwo, lineTwo, 0);
        matrixAux.set(lineOne, lineTwo, 1);
        matrixAux.set(lineTwo, lineOne, 1);
        return matrixAux.times(matrix);
    }

    public static Matrix diffLines(Matrix matrix, Integer lineOne, Integer lineTwo){
        double[] firstLine = matrix.getArray()[lineOne];
        double[] secondLine = matrix.getArray()[lineTwo];
        for(int i = 0; i < finalLine.length; i++){
            matrix.set(lineTwo, i, secondLine[i] - firstLine[i]);
        }
        return matrix;
    }

    public static Integer choosePivot(Matrix matrix, Integer e){
        Integer len = Math.min(matrix.getRowDimension(), matrix.getColumnDimension());
        List<Integer> pivotList = new ArrayList<Integer>();
        for(int i = e; i < len; i++){
            pivotList.add((int)Math.abs(matrix.get(i,e)));
        }
        Collections.shuffle(pivotList);
        return pivotList.get(0);
    }



    public static int[] doTheJob(){
        Integer len = Math.min(matrix_A.getRowDimension(), matrix_A.getColumnDimension());
        for (int i = 0; i < len; i++){
            Integer pivot = choosePivot(matrix_A, i);
            if (Math.abs(matrix_A.get(pivot, i)) <= epsilon){
                System.out.println("Matrice singulara!");
                System.exit(0);
            }
            matrix_A = switchLines(matrix_A, i, pivot);
            matrix_B = switchLines(matrix_B, i, pivot);
            for (int j = i + 1; i < len; i++){
                 Integer m = (int) (matrix_A.get(i,i) / matrix_A.get(j,i));
                 matrix_A = multiplyLine(matrix_A, j, m);
                 matrix_B = multiplyLine(matrix_B, j, m);
                 matrix_A = diffLines(matrix_A, i, j);
                 matrix_B = diffLines(matrix_B, i, j);
            }
        }
        int newVector[] = new int[len];
        for (int i = len - 1; i > -1; i--){
            int sum = 0;
            for(int j = i + 1; i < len; i++){
                sum += matrix_A.get(i,j) * newVector[j];
            }
            newVector[i] = (int)((matrix_B.get(0,i) - sum) / matrix_A.get(i,i));
        }
        return newVector;
    }

    public static void readFromConsole(){
        Scanner in = new Scanner(System.in);
        System.out.println("Insert the n dimension: ");
        N = 3;//in.nextInt();
        System.out.println("Insert e, where e is 10**(-e): ");
        E = 3;//in.nextInt();
        epsilon  = Math.pow(10, E);
        matrix_A = new Matrix(readMatrixFromFile("matrix_a",3,3));
        matrix_B = new Matrix(readMatrixFromFile("matrix_b",3,1));
        init_matrix_A = matrix_A;
        init_matrix_B = matrix_B;
    }

    public static double validate(int[] x, Matrix matrix1, Matrix matrix2){
        return matrix1.times(1).minus(matrix2).norm2();
    }

    public static void main(String[] args) {
        readFromConsole();
        int[] result = doTheJob();
        System.out.println("Solutie: ");
        for(int i = 0; i < result.length ; i++){
            System.out.print(result[i]);
        }

        System.out.println("Validare: ");
        double norm = validate(result, matrix_A, matrix_B);
        System.out.println(norm);

        System.out.println("NP Result: ");
        Matrix m = init_matrix_A.solve(init_matrix_B);
        System.out.println(m);

        System.out.println("First Result: ");
        System.out.println(m.norm2());

        System.out.println("NO Inv: ");
        Matrix inv = init_matrix_A.inverse();
        System.out.println(inv);

        System.out.println("Second result: ");
        System.out.println(m.minus(inv.times(init_matrix_B)).norm2());
    }
}
