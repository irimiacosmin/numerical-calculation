/*
	Made by: Irimia Cosmin & Bucevschi Alexandru [3A2]
*/
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class Tema2 {

    private static String prefix = "src/Tema3/";

    private static int N_A, N_B, N_APB, N_AMB;

    private static Map<Punct, Double> matrix_A, matrix_B, matrix_APB, matrix_AMB, test1, test2;

    private static List<Double> vector_A, vector_B, vector_APB, vector_AMB;

    static{
        matrix_A = new HashMap<>();
        matrix_B = new HashMap<>();
        matrix_APB = new HashMap<>();
        matrix_AMB = new HashMap<>();
        vector_A = new ArrayList<>();
        vector_B = new ArrayList<>();
        vector_APB = new ArrayList<>();
        vector_AMB = new ArrayList<>();

    }

    private static List<String> citire_fisier(String fisier){
        try {
            return Files.readAllLines(Paths.get(prefix + fisier));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static int memorizeThings(String filename, Map<Punct, Double> matrix, List<Double> vector)
    {
        ArrayList<String> file_aux = new ArrayList<>(citire_fisier(filename));
        int size_file_B = file_aux.size();
        int N = Integer.valueOf(file_aux.get(0));
        ArrayList<String> matrixString_B = new ArrayList<>(file_aux.subList(2 , size_file_B - N - 2));
        for(String val : matrixString_B){
            String[] split = val.split(", ");
            matrix.put(new Punct(Integer.valueOf(split[1]), Integer.valueOf(split[2])), Double.valueOf(split[0]));
        }
        ArrayList<String> vectorString_B = new ArrayList<>(file_aux.subList(size_file_B - N - 1 , size_file_B - 1));
        for(String val : vectorString_B){
            vector.add(Double.valueOf(val));
        }
        return N;
    }

    private static Map<Punct, Double> Add_Rare_Matrix(int N_A, Map<Punct, Double> matrix_A, int N_B, Map<Punct, Double> matrix_B){
        Map<Punct, Double> finalMatrix = new HashMap<>();
        if(N_A != N_B) return null;
        int i, j;
        Double valA, valB;
        Punct p;
        for(i=0; i<N_A; i++){
            for(j=0; j<N_B; j++){
                p = new Punct(i,j);
                valA = matrix_A.get(p);
                valB = matrix_B.get(p);
                if(valA == null) valA = 0.0;
                if(valB == null) valB = 0.0;
                if (valA + valB != 0.0)
                    finalMatrix.put(p, valA + valB);
            }
        }
        return finalMatrix;
    }
    private static Double multiplyTwoLists(List<Double> line, List<Double> column){
        Double sum = 0.0;
        for(int i=0;i<column.size();i++){
            sum += line.get(i) * column.get(i);
        }
        return sum;
    }

    private static Map<Punct, Double> Mul_Rare_Matrix(int N_A, Map<Punct, Double> matrix_A, int N_B, Map<Punct, Double> matrix_B){
        Map<Punct, Double> finalMatrix = new HashMap<>();
        if(N_A != N_B) return null;
        int i, j, k, recomLen;
        Double val,retVal;
        List<Double> line, column;
        List<Integer> recom;
        for(i=0; i<N_A; i++){
            line = new ArrayList<>();
            recom = new ArrayList<>();
            for (j = 0; j < N_A; j++) {
                val = matrix_A.get(new Punct(i, j));
                if (val != null) {
                    line.add(val);
                    recom.add(j);
                }
            }
            recomLen = recom.size();
            if(recomLen > 12){
                System.err.println("I found a line bigger than 12 elements!");
            }
            for(k=0; k<N_A; k++) {
                column = new ArrayList<>();
                for(j=0;j<recomLen; j++){
                    val = matrix_B.get(new Punct(recom.get(j), k));
                    if (val != null) {
                        column.add(val);
                    }else{
                        column.add(0.0);
                    }
                }
                retVal = multiplyTwoLists(line, column);
                if(retVal != 0.0)
                    finalMatrix.put(new Punct(i, k), retVal);
            }
        }
        return finalMatrix;
    }

    private static List<Double> Mul_Rare_Matrix_With_Vector(int N_A, Map<Punct, Double> matrix_A, List<Double> vector){
        Map<Punct, Double> finalMatrix = new HashMap<>();
        if(N_A != N_B) return null;
        int i, j, k, recomLen;
        Double val,retVal;
        List<Double> line, column;
        List<Integer> recom;
        for(i=0; i<N_A; i++){
            line = new ArrayList<>();
            recom = new ArrayList<>();
            for (j = 0; j < N_A; j++) {
                val = matrix_A.get(new Punct(i, j));
                if (val != null) {
                    line.add(val);
                    recom.add(j);
                }
            }
            recomLen = recom.size();
            if(recomLen > 12){
                System.err.println("I found a line bigger than 12 elements!");
            }

            column = new ArrayList<>();
            for(Integer elem: recom){
                column.add(vector.get(elem));
            }
            retVal = multiplyTwoLists(line, column);
            if(retVal != 0.0)
                finalMatrix.put(new Punct(i, 0), retVal);

        }
        List<Double> returnList = new ArrayList<>();
        for(i=0; i<N_A; i++){
            returnList.add(finalMatrix.get(new Punct(i, 0)));
        }

        return returnList;
    }

    public static void testare(){
        test1 = new HashMap<>();
        test2 = new HashMap<>();

        test1.put(new Punct(0,0), 1.0);
        test1.put(new Punct(0,1), 2.0);
        test1.put(new Punct(0,2), 3.0);
        test1.put(new Punct(1,0), 4.0);
        test1.put(new Punct(1,1), 5.0);
        test1.put(new Punct(1,2), 6.0);
        test1.put(new Punct(2,0), 7.0);
        test1.put(new Punct(2,1), 8.0);
        test1.put(new Punct(2,2), 9.0);

        test2.put(new Punct(0,1), 2.0);
        test2.put(new Punct(0,2), 1.0);
        test2.put(new Punct(1,0), 3.0);
        test2.put(new Punct(2,2), 6.0);

        //System.out.println(Add_Rare_Matrix(3, test1, 3 , test2));
        System.out.println(Mul_Rare_Matrix(3, test1, 3 , test2));
    }

    public static Map<Punct, Double> convertToRareMatrix(Double[][] matrix){
        Map<Punct, Double> returnVal = new HashMap<>();
        for(int i=0; i<matrix.length; i++){
            
        }
        return null;
    }

    public static void main(String[] argv){
        N_A = memorizeThings("a.txt", matrix_A, vector_A);
        N_B = memorizeThings("b.txt", matrix_B, vector_B);
        N_APB = memorizeThings("aplusb.txt", matrix_APB, vector_APB);
        N_AMB = memorizeThings("aorib.txt", matrix_AMB, vector_AMB);

        long startTime = System.nanoTime();
        Map<Punct, Double> m_APB = Add_Rare_Matrix(N_A, matrix_A, N_B, matrix_B);
        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        System.out.println("Adunarea matricilor a durat " + duration/1000000 + " ms.");
        System.out.println("E egala matricea calculata cu aplusb.txt? " + m_APB.equals(matrix_APB) + "\n");



        startTime = System.nanoTime();
        Map<Punct, Double> m_AMB = Mul_Rare_Matrix(N_A, matrix_A, N_B, matrix_B);
        endTime = System.nanoTime();
        duration = (endTime - startTime);
        System.out.println("Inmultirea matricilor a durat " + duration/1000000 + " ms.");
        System.out.println("E egala matricea calculata cu aorib.txt? " + m_AMB.equals(matrix_AMB) + "\n");



        startTime = System.nanoTime();
        List<Double> vector = new ArrayList<>();
        for(Double i = Double.valueOf(N_A); i>=1.0; i--){
            vector.add(i);
        }
        List<Double> mat_Vector = Mul_Rare_Matrix_With_Vector(N_A, matrix_A, vector);
        endTime = System.nanoTime();
        duration = (endTime - startTime);
        System.out.println("Inmultirea matricii cu vectorul a durat " + duration/1000000 + " ms.");
        System.out.println("E egal vectorul cu cel initial? " + mat_Vector.equals(vector_A) + "\n");



        Double[][] matrice = new Double[5][5];
        matrice[0][0] = 102.5;
        matrice[0][1] = 0.0;
        matrice[0][2] = 2.5;
        matrice[0][3] = 0.0;
        matrice[0][4] = 0.0;

        matrice[1][0] = 3.5;
        matrice[1][1] = 104.88;
        matrice[1][2] = 1.05;
        matrice[1][3] = 0.0;
        matrice[1][4] = 0.33;

        matrice[2][0] = 0.0;
        matrice[2][1] = 0.0;
        matrice[2][2] = 100.0;
        matrice[2][3] = 0.0;
        matrice[2][4] = 0.0;

        matrice[3][0] = 0.0;
        matrice[3][1] = 1.3;
        matrice[3][2] = 0.0;
        matrice[3][3] = 101.3;
        matrice[3][4] = 0.0;

        matrice[4][0] = 0.73;
        matrice[4][1] = 0.0;
        matrice[4][2] = 0.0;
        matrice[4][3] = 1.5;
        matrice[4][4] = 102.23;

        Map<Punct, Double> mts = convertToRareMatrix(matrice);





    }


}
