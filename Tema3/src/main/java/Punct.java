/*
	Made by: Irimia Cosmin & Bucevschi Alexandru [3A2]
*/
import java.util.Objects;

public class Punct {
    private int x;
    private int y;

    public Punct(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "Punct{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Punct punct = (Punct) o;
        return x == punct.x &&
                y == punct.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

}
